const gulp = require('gulp');

const paths = {
	styles: `${__dirname}/src/style.less`,
	dist: `${__dirname}/dist`,
	scripts: `${__dirname}/src/main.js`,
	stylesWatch: `${__dirname}/src/**/*.less`,
	scriptsWatch: `${__dirname}/src/**/*js`
};

//Import tasks
const styles = require('./tasks/styles')(gulp, paths.styles, paths.dist);
const scripts = require('./tasks/babel');
const eslint = require('./tasks/eslint')(gulp, paths.scripts);

//Compile tasks
gulp.task('compile', ['compile:styles', 'compile:scripts']);
gulp.task('compile:styles', [], styles);
gulp.task('compile:scripts', ['eslint'], scripts(gulp, paths.scripts, paths.dist));

//ESlint task
gulp.task('eslint', [], eslint);

//Watch tasks
gulp.task('watch', ['compile'], function() {
	gulp.watch(paths.stylesWatch, ['compile:styles']);
	gulp.watch(paths.scriptsWatch, ['compile:scripts']);
});

//Default task
gulp.task('default', ['watch']);
