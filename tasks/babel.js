module.exports = (gulp, scripts, dist) => {
    return () => {

        const source = require('vinyl-source-stream');
        const rename = require('gulp-rename');
        const gutil = require('gulp-util');
        const buffer = require('vinyl-buffer');
        const babelify = require('babelify');
        const browserify = require('browserify');
        const uglify = require('gulp-uglify');
        

        let bundle = (bundler) => {
          bundler
            .bundle()
            .pipe(source('bundled-app.js'))
            .pipe(buffer())
            .pipe(rename('graph.js'))
            .pipe(gutil.env.env === 'prod' ? uglify() : gutil.noop())
            .pipe(gulp.dest(dist))
            .on('end', () => gutil.log(gutil.colors.green('==> Successful Bundle!')));
        };

        let bundler = browserify(scripts, { debug: true })
            .transform(babelify, { presets: ['es2015']} );

        return bundle(bundler);

    };

};