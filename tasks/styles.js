module.exports = (gulp, styles, dist) => {
    return () => {
        const less = require('gulp-less');
        const sourceMaps = require('gulp-sourcemaps');
        const plumber = require('gulp-plumber');
        const notify = require('gulp-notify');
        const autoPrefixer = require('gulp-autoprefixer');
        const gutil = require('gulp-util');

        return gulp.src(styles)
            .pipe(gutil.env.env === 'prod' ? gutil.noop() : sourceMaps.init())
            .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
            .pipe(less())
            .pipe(autoPrefixer({
                browsers: ['> 0%'],
                cascade: false
            }))
            .pipe(gutil.env.env === 'prod' ? gutil.noop() : sourceMaps.write())
            .pipe(gulp.dest(dist));
    };
};