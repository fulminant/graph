# D3 Graph


### Graph plugin

First of all we need install dependencies with command
```
npm i
```

Next, for start graph run
```
npm start
```
For develop graph (in a separate terminal) run
```
gulp
```
For compile production version run
```
npm run build
```

### Technology Stack

* ECMAScript6
* D3
* Gulp
* NodeJS
* Browserify
* Babel
* LESS

### Requirements

* NodeJS 4.+
* npm 3.+

### Gulp tasks &amp; npm tasks

Serve application to browser

```
npm start
```

Compiling options

```
gulp compile:styles       # compile and move LESS files
gulp compile:scripts      # compile and bundle all es6 files
gulp watch                # compile and watch es6/styles files
```

For use graph as plugin you need
------
* Build js and css files with
```
npm run build
```
* Include `graph.js` and `style.css` in your html file

* Add block with class
```
<div class="myGraph"></div>
```
* _important_ Add `width` and `height` attributes
```
<div class="myGraph" style="width: 1000px; height: 550px;"></div>
```
* Init graph in your script file
```
var myGraph = new Graph('.my-own-graph', 'data', options);
```
Where `.my-own-graph` is container class, and `data` is name or path to `json` file with data, or array of data, and options is object with graph options. If you don't use any options third argument should be empty object.

* _important_ Data should be in next format
```
[{
    "name": 'Test name',
    "value": 50
}]
```
* _important_ Second variation of data should be in next format
```
[{
    "name": "1",
    "value": 20,
    "year": "2005",
    "title": "Test Title",
    "subtitle": "Test Sub-Title",
    "text": "Discover the world's top universities for art"
}]
```
* _important_ graph support next options when it create
```
options = {
    removeElements: null,
    hoverActiveElements: ['lines', 'xAxis', 'circles'],
    clickActiveElements: ['lines', 'circles'],
    pathBgGradations: ['0%', '100%'],
    areaBgGradations: ['0%', '75%'],
    linesBgGradations: ['0%', '45%', '65%', '100%'],
    leftShadowBgGradations: ['20%', '50%', '100%'],
    rightShadowBgGradations: ['0%', '50%', '80%'],
    circleShadowColor: '#00dac0',
    circleShadowHoverColor: '#00dac0',
    extendedTooltip: false
};
```


* Graph methods
  * update - method to update Graph with new data or options.

    ```
    myGraph.update('data', options);
    ```

  * destroy - method to remove all Graph from DOM.
    ```
    myGraph.destroy();
    ```
  * getRandomData method to get random data for test graph.
    Can be called with one argument {Number} of quantity if generated elements.
    ```
    myGraph.getRandomData();
    ```


* _important_ graph support next options when it update
```
options = {
    removeElements: null,
    hoverActiveElements: ['lines', 'xAxis', 'circles'],
    clickActiveElements: ['lines', 'circles'],
    extendedTooltip: false
};
```
